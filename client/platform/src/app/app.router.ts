import { Routes, RouterModule }  from "@angular/router";
import { NgModule } from "@angular/core";

import { PageRoutes, PageComponents } from './pages/page.router';
import { WidgetComponents } from './widgets/widget.list';

export const AppComponents = [
    ...PageComponents,
    ...WidgetComponents
];

export const AppRoutes : Routes = [
    ...PageRoutes
];

@NgModule({
  imports: [ RouterModule.forRoot(AppRoutes)],
  exports: [ RouterModule ]
})

export class AppRouterModule{}
