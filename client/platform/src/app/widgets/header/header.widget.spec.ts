import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule,NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Observable } from 'rxjs/Observable';

import { HeaderWidget } from './header.widget';

describe('HeaderWidget', () => {
  let component: HeaderWidget;
  let fixture: ComponentFixture<HeaderWidget>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderWidget],
      imports:[RouterTestingModule,
              BrowserAnimationsModule ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(HeaderWidget);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

