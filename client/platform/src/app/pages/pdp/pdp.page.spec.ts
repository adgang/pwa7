import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule,NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Observable } from 'rxjs/Observable';

import { PdpPage } from './pdp.page';

describe('PdpPage', () => {
  let component: PdpPage;
  let fixture: ComponentFixture<PdpPage>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ PdpPage],
      imports:[RouterTestingModule,
              BrowserAnimationsModule ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(PdpPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

